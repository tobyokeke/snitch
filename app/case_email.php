<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class case_email extends Model
{
    protected $primaryKey = 'ceid';
    protected $table = 'case_emails';
}
