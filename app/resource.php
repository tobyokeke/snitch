<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class resource extends Model
{
    protected $primaryKey = 'resid';
    protected $table = 'resources';
}
