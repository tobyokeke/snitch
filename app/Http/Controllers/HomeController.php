<?php

namespace App\Http\Controllers;

use App\agency;
use App\cases;
use App\location;
use App\report;
use App\resource;
use App\snitch;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	return view('dashboard');
    }

	public function dashboard() {

		$reports = report::all();
		$snitches = snitch::all();
		$cases = cases::all();
		$agencies = agency::all();
		$resources = resource::all();
		$locations = location::all();


		return view('dashboard',[
			'reports'  => $reports,
			'snitches' => $snitches,
			'cases'    => $cases,
			'agencies' => $agencies,
			'resources'=> $resources,
			'locations'=> $locations
		]);
	}

	public function reports() {
    	$reports = report::all();

		return view('reports',[
			'reports' => $reports,

		]);
    }
}
