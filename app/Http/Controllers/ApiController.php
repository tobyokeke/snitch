<?php

namespace App\Http\Controllers;

use App\location;
use App\report;
use App\resource;
use Illuminate\Http\Request;

class ApiController extends Controller
{
	public function addReport( Request $request ) {

		try{
			$webhook_secret = 'LT9APDM9FWHR3X9QHEKZQGK7PHN2TCQE';

			if ($request->input('secret') !== $webhook_secret)
			{
				header('HTTP/1.1 403 Forbidden');
				echo "Invalid webhook secret";
			}
			else
			{
				if ($request->input('event') == 'incoming_message')
				{
					$content = $_POST['content'];
					$from_number = $_POST['from_number'];

					$report = new report();
					$report->phone = $from_number;
					$report->details = $content;
					$report->save();

					$content = explode(' ',$content);

					header("Content-Type: application/json");
					echo json_encode(array(
						'messages' => array(
							array('content' => "Your report has been received.")
						)
					));

				}
			}
		}catch(\Exception $exception){
			header("Content-Type: application/json");
			echo json_encode(array(
				'messages' => array(
					array('content' => "An error occurred. Please resend your message.")
				)
			));

		}
	}


	public function getReports() {
		$reports = report::all();
		return count($reports);
	}
}
