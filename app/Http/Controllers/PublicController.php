<?php

namespace App\Http\Controllers;

use App\report;
use App\snitch;
use Illuminate\Http\Request;

class PublicController extends Controller
{
	public function index() {

		$reports = report::all();
		$snitches = snitch::all();


		return view('welcome',[
			'reports' => $reports,
			'snitches' => $snitches
		]);
    }

	public function postSnitch( Request $request ) {

		try{

			$fileurl = "";
			$imageurl = "";
			if($request->hasFile('file')){
				$filename = $request->file('file')->getClientOriginalName();
				$request->file('file')->move('uploads/',$filename);
				$fileurl = url('uploads/' . $filename);
			}

			if($request->hasFile('file')){
				$filename = $request->file('file')->getClientOriginalName();
				$request->file('file')->move('uploads/',$filename);
				$imageurl = url('uploads/' . $filename);
			}

			$snitch = new snitch();
			$snitch->name = $request->input('name');
			$snitch->location = $request->input('location');
			$snitch->image = $imageurl;
			$snitch->file = $fileurl;
			$snitch->save();

			$request->session()->flash('success','Snitch Saved. Thanks for helping change the world');

			return redirect('/');

		}catch (\Exception $exception){
			$request->session()->flash('error','Sorry something went wrong. Try again');
			return redirect('/');
		}
    }

}
