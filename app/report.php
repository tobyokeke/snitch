<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class report extends Model
{
    protected $primaryKey = 'rid';
    protected $table = 'reports';

	public function Location() {
		return $this->belongsTo(location::class,'lid','lid');
    }
}
