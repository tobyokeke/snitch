<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class agency extends Model
{
    protected $primaryKey = 'aid';
    protected $table = 'agencies';
}
