<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class location extends Model
{
    protected $primaryKey = 'lid';
    protected $table = 'locations';

	public function reports() {
		return $this->hasMany(report::class,'rid','rid');
    }
}
