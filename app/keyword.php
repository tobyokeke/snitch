<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class keyword extends Model
{
    protected $primaryKey = 'kid';
    protected $table = 'keywords';
}
