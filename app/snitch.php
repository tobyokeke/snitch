<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class snitch extends Model
{
    protected $primaryKey = 'sid';
    protected $table = 'snitches';
}
