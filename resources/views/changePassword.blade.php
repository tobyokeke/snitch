@extends('layouts.app')

@section('content')


    @include('notification')
    <div class="row" style="margin-top: 50px;">

        <div class="col s12 m6 l6 offset-l3 offset-m3">
            <div class="card white darken-1">
                <div class="card-content z-depth-5 ">
                    <div align="center">
                        <span class="card-title teal-text">CHANGE PASSWORD</span>

                    </div>
                    <div class="row login">

                        <form method="post" action="{{url('change-password')}}">
                            {{ csrf_field() }}

                            <div class="input-field s12">
                                <label for="oldpassword" class="col-md-4 control-label">Old Password</label>

                                <input id="oldpassword" type="password" class="form-control" name="oldpassword" required>

                            </div>

                            <div class="input-field s12">
                                <label for="password" class="col-md-4 control-label">Password</label>

                                <input id="password" type="password" class="form-control" name="password" required>


                            </div>


                            <div class="input-field s12">
                                <label for="confirmpassword" class="col-md-4 control-label">Confirm Password</label>

                                <input id="confirmpassword" type="password" class="form-control" name="confirmpassword" required>

                            </div>

                            <br>
                            <div class="col s12 right-align m-t-sm">
                                <button type="submit" class="btn btn-primary">
                                    Change
                                </button>

                            </div>                        </form>

                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection