@extends('layouts.app')

@section('content')

    <div class="col s12 m8 l9">

        <div class="col m12 l12 s12">
            <div id="chart" style="width:100%"></div>
        </div>

        <br><br><br>
        <div class="row">

            <table class="striped">
                <thead>
                <tr>
                    <th>From</th>
                    <th>Content</th>
                    <th>Matched Keywords</th>
                    <th>View</th>
                </tr>
                </thead>

                <tbody>

                @foreach($reports as $report)
                <tr>
                    <td>{{$report->phone}}</td>
                    <td>{{$report->details}}</td>
                    <td>No match</td>
                    <td>
                        <a href="{{url('view-report')}}" class="btn teal">View</a>
                    </td>
                </tr>

                @endforeach
                </tbody>
            </table>

        </div>
    </div>




@endsection