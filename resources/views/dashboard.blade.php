@extends('layouts.app')

@section('content')


        <div class="col s12 m8 l9">
            <br><br><br>
            <div class="row">

                <div class="col s4">
                    <div class="card-panel teal" style="width:150px; height:150px; position:absolute; top: 5%; z-index: 1;">
                        <span class="white-text center-align"><i class="fa fa-users fa-5x"></i></span>
                    </div>

                    <div class="card horizontal z-depth-5">
                        <div class="card-stacked">
                            <div class="card-content">
                                <span class="right-align card-title">Reports</span>
                                <h4 class="right-align" id="reportCount">{{count($reports)}}</h4>
                            </div>
                            <div class="card-action">
                                <a href="{{url('reports')}}">View Reports</a>
                            </div>
                        </div>

                    </div>
                    <!-- Promo Content 1 goes here -->
                </div>

                <div class="col s4">
                    <div class="card-panel" style="width:150px; height:150px; position:absolute; top: 5%; z-index: 1; background-color:#b0395c;">
                        <span class="white-text center-align"><i class="fa fa-home fa-5x"></i></span>
                    </div>


                    <div class="card horizontal z-depth-5">
                        <div class="card-stacked">
                            <div class="card-content">
                                <span class="right-align card-title">Snitches</span>
                                <h4 class="right-align">{{count($snitches)}}</h4>
                            </div>
                            <div class="card-action">
                                <a href="{{url('snitches')}}">View Snitches</a>

                            </div>
                        </div>

                    </div>
                    <!-- Promo Content 1 goes here -->
                </div>

                <div class="col s4">
                    <div class="card-panel" style="width:150px; height:150px; position:absolute; top: 5%; z-index: 1; background-color:#425b44;">
                        <span class="white-text center-align"><i class="fa fa-id-badge fa-5x"></i></span>
                    </div>


                    <div class="card horizontal z-depth-5">
                        <div class="card-stacked">
                            <div class="card-content">
                                <span class="right-align card-title">Resources</span>
                                <h4 class="right-align">{{count($resources)}}</h4>
                            </div>
                            <div class="card-action">
                                <a href="{{url('resources')}}">View Resources</a>
                                <a href="{{url('add-resource')}}">Add Resources</a>
                            </div>
                        </div>

                    </div>
                    <!-- Promo Content 1 goes here -->
                </div>
            </div>

            <div class="row">

                <div class="col s4">
                    <div class="card-panel teal" style="width:150px; height:150px; position:absolute; top: 5%; z-index: 1;">
                        <span class="white-text center-align"><i class="fa fa-users fa-5x"></i></span>
                    </div>

                    <div class="card horizontal z-depth-5">
                        <div class="card-stacked">
                            <div class="card-content">
                                <span class="right-align card-title">Agencies</span>
                                <h4 class="right-align">{{count($agencies)}}</h4>
                            </div>
                            <div class="card-action">
                                <a href="{{url('agencies')}}">View Agencies</a>
                                <a href="{{url('add-agency')}}">Add</a>
                            </div>
                        </div>

                    </div>
                    <!-- Promo Content 1 goes here -->
                </div>

                <div class="col s4">
                    <div class="card-panel" style="width:150px; height:150px; position:absolute; top: 5%; z-index: 1; background-color:#b0395c;">
                        <span class="white-text center-align"><i class="fa fa-home fa-5x"></i></span>
                    </div>


                    <div class="card horizontal z-depth-5">
                        <div class="card-stacked">
                            <div class="card-content">
                                <span class="right-align card-title">Cases</span>
                                <h4 class="right-align">{{count($cases)}}</h4>
                            </div>
                            <div class="card-action">
                                <a href="{{url('cases')}}">View Cases</a>
                                <a href="{{url('add-case')}}">Add Case</a>

                            </div>
                        </div>

                    </div>
                    <!-- Promo Content 1 goes here -->
                </div>

                <div class="col s4">
                    <div class="card-panel" style="width:150px; height:150px; position:absolute; top: 5%; z-index: 1; background-color:#425b44;">
                        <span class="white-text center-align"><i class="fa fa-id-badge fa-5x"></i></span>
                    </div>


                    <div class="card horizontal z-depth-5">
                        <div class="card-stacked">
                            <div class="card-content">
                                <span class="right-align card-title">Locations</span>
                                <h4 class="right-align">{{count($locations)}}</h4>
                            </div>
                            <div class="card-action">
                                <a href="{{url('locations')}}">View Locations</a>
                            </div>
                        </div>

                    </div>
                    <!-- Promo Content 1 goes here -->
                </div>
            </div>


            <div class="row">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3940.2843856067334!2d7.501957414486105!3d9.037802093514754!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x104e0bed2344ed7b%3A0x82adf1bba69930e2!2sAguleri+St%2C+Garki%2C+Abuja%2C+Nigeria!5e0!3m2!1sen!2s!4v1505947561010" width="900" height="330" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>


        </div>


@endsection