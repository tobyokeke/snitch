<!--
	Author: W3layouts
	Author URL: http://w3layouts.com
	License: Creative Commons Attribution 3.0 Unported
	License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>SNITCH 4 GOOD</title>
    <!-- Meta tag Keywords -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Scholarly web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
        function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!--// Meta tag Keywords -->
    <!-- css files -->
    <link rel="stylesheet" href="css/bootstrap.css"> <!-- Bootstrap-Core-CSS -->
    <link rel="stylesheet" href="css/style.css" type="text/css" media="all" /> <!-- Style-CSS -->
    <link rel="stylesheet" href="css/font-awesome.css"> <!-- Font-Awesome-Icons-CSS -->
    <link rel="stylesheet" href="css/swipebox.css">
    <link rel="stylesheet" href="css/jquery-ui.css" />
    <!-- //css files -->
    <!-- online-fonts -->
    <link href="//fonts.googleapis.com/css?family=Exo+2:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=cyrillic,latin-ext" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i&amp;subset=latin-ext" rel="stylesheet">
    <!-- //online-fonts -->
    <script type="text/javascript" src="{{url('backend/js/jquery.js')}}"></script>
    <script type="text/javascript" src="{{url('backend/js/jquery-ui.min.js')}}"></script>
</head>
<body>

<!-- banner -->
<div class="main_section_agile" id="home">
    <div class="agileits_w3layouts_banner_nav">
        <nav class="navbar navbar-default">
            <div class="navbar-header navbar-left">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <h1><a class="navbar-brand" href="index.html"><i class="fa fa-leanpub" aria-hidden="true"></i>SNITCH</a></h1>

            </div>
            <div class="w3layouts_header_right">
                <form action="#" method="post">
                    <input name="Search here" type="search" placeholder="Search" required="">
                    <input type="submit" value="">
                </form>
            </div>
            <ul class="agile_forms">
                <li><a class="active" href="#" data-toggle="modal" data-target="#myModal2"><i class="fa fa-sign-in" aria-hidden="true"></i> Sign In</a> </li>
                <li><a href="#" data-toggle="modal" data-target="#myModal3"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Sign Up</a> </li>
            </ul>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
                <nav class="link-effect-2" id="link-effect-2">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="index.html" class="effect-3">HOME</a></li>
                        <li><a href="#about" class="effect-3 scroll">ABOUT</a></li>
                        <li><a href="#getinfo" class="effect-3 scroll">GET INFO</a></li>
                        <li><a href="#offerinfo" class="effect-3 scroll">OFFER INFO</a></li>
                        <li><a href="#team" class="effect-3 scroll">FORUM</a></li>
                        <li><a href="#mail" class="effect-3 scroll">CONTACT US</a></li>

                    </ul>
                </nav>

            </div>
        </nav>
        <div class="clearfix"> </div>
    </div>
</div>
<!-- banner -->



@yield('content')
<!-- contact -->
<div id="mail" class="contact">
    <div class="container">
        <h3 class="w3l-title">Mail Us</h3>
        <div class="w3layouts_header">
            <p><i class="fa fa-graduation-cap" aria-hidden="true"></i></p>
        </div>
        <div class="agile_banner_bottom_grids">
            <div class="col-md-4 col-xs-4 w3_agile_contact_grid">
                <div class="agile_contact_grid_left">
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                </div>
                <div class="agile_contact_grid_right agilew3_contact">
                    <h4>Address</h4>
                    <p>No 50, Adetokunbo Ademola Crescent,</p>
                    <p>Wuse 2, Abuja.</p>
                </div>
            </div>
            <div class="col-md-4 col-xs-4 w3_agile_contact_grid">
                <div class="agile_contact_grid_left agileits_w3layouts_left">
                    <i class="fa fa-mobile" aria-hidden="true"></i>
                </div>
                <div class="agile_contact_grid_right agileits_w3layouts_right">
                    <h4>Phone</h4>
                    <p>+234 808 1234504</p>
                </div>
            </div>
            <div class="col-md-4 col-xs-4 w3_agile_contact_grid">
                <div class="agile_contact_grid_left agileits_w3layouts_left1">
                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                </div>
                <div class="agile_contact_grid_right agileits_w3layouts_right1">
                    <h4>Email</h4>
                    <p><a href="mailto:info@example.com">info@snitch4good.com</a>
                    </p>
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="clearfix"> </div>
        </div>
        <div class="w3l-form">
            <h3 class="w3l-title">Get In Touch</h3>
            <div class="contact-grid1">
                <div class="contact-top1">
                    <form action="#" method="post">
                        <div class="col-md-6 col-xs-6 wthree_contact_left_grid">
                            <label>Name*</label>
                            <input type="text" name="Name" placeholder="Name" required="">
                            <label>E-mail*</label>
                            <input type="email" name="E-mail" placeholder="E-mail" required="">
                        </div>
                        <div class="col-md-6 col-xs-6 wthree_contact_left_grid">
                            <label>Phone Number*</label>
                            <input type="text" name="number" placeholder="Phone Number" required="">
                            <label>Subject*</label>
                            <input type="text" name="subject" placeholder="Subject" required="">
                        </div>
                        <div class="form-group">
                            <label>Message*</label>
                            <textarea placeholder name="Message" required=""></textarea>
                        </div>
                        <input type="submit" value="Send">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="map"></div>
<!-- footer -->
<div class="footer">
    <div class="container">
        <div class="wthree_footer_grid_left">
            <div class="col-md-3 col-xs-3 wthree_footer_grid_left1">
                <h4>About Us</h4>
                <p>
                    Snitch is an initiative aimed at curbing child labour by providing the enough information
                    to raise awareness and enable innovators create awesome solutions for the problem.
                </p>
            </div>
            <div class="col-md-3 col-xs-3 wthree_footer_grid_left1">
                <h4>Navigation</h4>
                <ul>
                    <li class="active"><a href="index.html" class="effect-3">HOME</a></li>
                    <li><a href="#about" class="effect-3 scroll">ABOUT</a></li>
                    <li><a href="#getinfo" class="effect-3 scroll">GET INFO</a></li>
                    <li><a href="#offerinfo" class="effect-3 scroll">OFFER INFO</a></li>
                    <li><a href="#team" class="effect-3 scroll">FORUM</a></li>
                    <li><a href="#mail" class="effect-3 scroll">CONTACT US</a></li>

                </ul>
            </div>
            <div class="col-md-3 col-xs-3 wthree_footer_grid_left1 w3l-3">
                <h4>Others</h4>
                <ul>

                    <li><i class="fa fa-angle-double-right" aria-hidden="true"></i><a href="#">Media</a></li>
                    <li><i class="fa fa-angle-double-right" aria-hidden="true"></i><a href="#">Mobile Apps</a></li>
                    <li><i class="fa fa-angle-double-right" aria-hidden="true"></i><a href="#">Privacy Policy</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-xs-3 wthree_footer_grid_left1 wthree_footer_grid_right1">
                <h4>Contact Us</h4>
                <ul>
                    <li><i class="fa fa-envelope-o" aria-hidden="true"></i><a href="mailto:info@example.com">info@snitch4good.com</a></li>
                    <li><i class="fa fa-phone" aria-hidden="true"></i>+2348081234504</li>
                </ul>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<div class="w3layouts_copy_right">
    <div class="container">
        <p>© 2017 Snitch4good. All rights reserved | Developed with love from <a href="http://civicilab.com">Civic Innovation Lab</a></p>
    </div>
</div>
<!-- //footer -->

<!-- js-scripts -->
<!-- js-files -->
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="js/bootstrap.js"></script> <!-- Necessary-JavaScript-File-For-Bootstrap -->
<!-- //js-files -->
<!-- Baneer-js -->

<!-- Map-JavaScript -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>
<script type="text/javascript">
    google.maps.event.addDomListener(window, 'load', init);
    function init() {
        var mapOptions = {
            zoom: 11,
            center: new google.maps.LatLng(40.6700, -73.9400),
            styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":17}]}]
        };
        var mapElement = document.getElementById('map');
        var map = new google.maps.Map(mapElement, mapOptions);
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(40.6700, -73.9400),
            map: map,
        });
    }
</script>
<!-- //Map-JavaScript -->

<!-- smooth scrolling -->
<script src="js/SmoothScroll.min.js"></script>
<!-- //smooth scrolling -->
<!-- stats -->
<script type="text/javascript" src="js/numscroller-1.0.js"></script>
<!-- //stats -->
<!-- moving-top scrolling -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        /*
            var defaults = {
            containerID: 'toTop', // fading element id
            containerHoverID: 'toTopHover', // fading element hover id
            scrollSpeed: 1200,
            easingType: 'linear'
            };
        */
        $().UItoTop({ easingType: 'easeOutQuart' });
    });
</script>
<a href="#home" class="scroll" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<!-- //moving-top scrolling -->
<!-- gallery popup -->
<script src="js/jquery.swipebox.min.js"></script>
<script type="text/javascript">
    jQuery(function($) {
        $(".swipebox").swipebox();
    });
</script>
<!-- //gallery popup -->
<!--/script-->
<script src="js/simplePlayer.js"></script>
<script>
    $("document").ready(function() {
        $("#video").simplePlayer();
    });
</script>
<!-- //Baneer-js -->
<!-- Calendar -->
<script src="js/jquery-ui.js"></script>
<script>
    $(function() {
        $( "#datepicker" ).datepicker();
    });
</script>
<!-- //Calendar -->

<!-- //js-scripts -->
</body>
</html>