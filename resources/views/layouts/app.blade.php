<!DOCTYPE html>
<html lang="en">

<!--================================================================================
	Item Name: Materialize - Material Design Admin Template
	Version: 3.1
	Author: GeeksLabs
	Author URL: http://www.themeforest.net/user/geekslabs
================================================================================ -->


<!-- Mirrored from demo.geekslabs.com/materialize-v1.0/user-profile-page.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 14 Sep 2017 08:37:20 GMT -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">


    <!-- Favicons-->
    <link rel="icon" href="images/favicon/favicon-32x32.png" sizes="32x32">
    <!-- Favicons-->
    <link rel="apple-touch-icon-precomposed" href="images/favicon/apple-touch-icon-152x152.png">
    <!-- For iPhone -->
    <meta name="msapplication-TileColor" content="#00bcd4">
    <meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png">
    <!-- For Windows Phone -->


    <!-- CORE CSS-->

    <link href="{{url('backend/css/materialize.css')}}" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="{{url('backend/css/style.css')}}" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link rel="stylesheet" href="{{url('backend/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{url('backend/css/font-awesome.css')}}">
    <!-- Custome CSS-->


    <link href="{{ asset('backend/css/c3.min.css') }}" rel="stylesheet">
    <link href="{{ url('css/style.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="{{url('css/jquery-ui.min.css')}}">
    <script type="text/javascript" src="{{url('backend/js/jquery.js')}}"></script>
    <script type="text/javascript" src="{{url('backend/js/jquery-ui.min.js')}}"></script>
    <script src="{{url('backend/js/d3.min.js')}}"></script>
    <script src="{{url('backend/js/c3.min.js')}}"></script>




</head>

<body style="background-color: #e9e9e9;">

<div class="row">

    <div class="col s12 m4 l3 ">
        <ul id="slide-out" style="background-color: #ffb71c" class="side-nav fixed darken-1">
            <li>
                <div class="user-view" style="background-color:#002147; color: lightblue;">
                    <div class="background">
                        <a href="{{url('/')}}">
                            <img class="circle" src="{{url('backend/images/logo.png')}}">
                        </a>
                    </div>
                    <div align="center">
                        <a href="#!user"><img class="circle" src="{{url('backend/images/profile.png')}}"></a>
                    </div>
                    <div style="padding:20px;">
                        @if(!Auth::guest())
                        <a href="#!name"><p class="white-text name">Welcome
                                {{Auth::user()->name}}
                            </p></a>
                        <a href="#!email"><p class="white-text email">{{Auth::user()->email}}</p></a>
                        @endif
                    </div>

                </div>
            </li>

            <li class="active"><a href="{{url('dashboard')}}"><i class="fa fa-user-circle fa-2x" aria-hidden="true"></i>Dashboard</a></li>
            <li ><a href="{{url('reports')}}"><i class="fa fa-users fa-2x"></i>REPORTS & CASES</a></li>
            <li><a href="{{url('snitches')}}"><i class="fa fa-id-badge fa-2x"></i>SNITCHES</a></li>
            <li><a  href="{{url('agencies')}}"><i class="fa fa-home fa-2x"></i>AGENCIES</a></li>
            <li ><a href="{{url('resources')}}"><i class="fa fa-cog fa-2x"></i>RESOURCES</a></li>



            <li><div class="divider"></div></li>
            <li><a class="waves-effect" href="{{url('logout')}}"><i class="fa fa-power-off fa-2x"></i>Logout</a></li>


        </ul>
    </div>


@yield('content')

</div>


<script>


    function updateReport(){
        $.ajax({
            url: "{{url('api/get-reports')}}",
            method: "get",
            success: function (response) {
                $('#reportCount').text(response);
            },
            error: function (error) {
                console.log(error);
            }
        });

    }


    $(document).ready(function () {

        setInterval(updateReport,5000);

    });

</script>

<!--materialize js-->
<script type="text/javascript" src="{{url('backend/js/materialize.min.js')}}"></script>
<!--prism-->

<!-- google map api -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAAZnaZBXLqNBRXjd-82km_NO7GUItyKek"></script>
<!--google map-->
<script>
    $(document).ready(function() {
        Materialize.updateTextFields();
    });

</script>

</body>


</html>