@extends('layouts.frontend')

@section('content')

    <div class="about-bottom">
        <div class="col-md-6 w3l_about_bottom_left">
            <div class="video-grid-single-page-agileits">
                <div data-video="81HCICAv6MA" id="video"> <img src="images/banner2.jpg" alt="" class="img-responsive" /> </div>
            </div>
            <div class="w3l_about_bottom_left_video">
                <h4>watch our video</h4>
            </div>
        </div>
        <div class="col-md-6 w3l_about_bottom_right one">
            <div class="abt-w3l">
                <div class="header-w3l">
                    <h2>SNITCH NOW</h2>
                    <h4>For the good of society</h4>
                    <form action="{{url('snitch')}}" method="post" class="mod2">
                        <div class="col-md-6 col-xs-6 w3l-left-mk">
                            <ul>
                                <li class="text">Name:  </li>
                                <li class="agileits-main"><i class="fa fa-user-o" aria-hidden="true"></i><input name="name" type="text" required=""></li>
                                <li class="text">Audio/ Video (if any)  :  </li>
                                <li class="agileits-main"><i class="fa fa-user-o" aria-hidden="true"></i><input name="file" type="text" required=""></li>
                            </ul>
                        </div>
                        <div class="col-md-6 col-xs-6 w3l-right-mk">
                            <ul>
                                <li class="text">Image :  </li>
                                <li class="agileits-main"><i class="fa fa-calendar" aria-hidden="true"></i><input name="text" type="text" required=""></li>

                                <li class="text">Location  :  </li>
                                <li class="agileits-main"><i class="fa fa-user-o" aria-hidden="true"></i><input name="location" type="text" required=""></li>

                            </ul>
                        </div>

                        <div class="col-md-12 col-xs-12">
                            <ul>
                                <li class="text">Story :  </li>
                                <li class="agileits-main"><i class="fa fa-calendar" aria-hidden="true"></i>
                                    <textarea class="form-control" name="story"   required=""></textarea>
                                </li>
                            </ul>
                        </div>

                        <div class="clearfix"></div>
                        <div class="agile-submit">
                            <input type="submit" value="submit">
                        </div>
                    </form>

                    <div align="center" style=" background-color: #263062;margin: 20px;">
                        <img style="height:60px; width: auto;  padding: 5px;" src="{{url('images/civiclab.png')}}">
                        <img style="height:60px; width: auto; padding: 5px;" src="{{url('images/ilo.png')}}">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- //banner -->
    <!-- Modal1 -->
    <div class="modal fade" id="myModal2" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="signin-form profile">
                        <h3 class="agileinfo_sign">Sign In</h3>

                        <div class="login-form">
                            <form action="{{url('login')}}" method="post">
                                {{csrf_field()}}
                                <input type="email" placeholder="E-mail" name="email" value="{{ old('email') }}" required="">
                                <input type="password" name="password" placeholder="Password" required="">
                                <div class="tp">
                                    <input type="submit" value="Sign In">
                                </div>
                            </form>
                        </div>
                        <div class="login-social-grids">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-rss"></i></a></li>
                            </ul>
                        </div>
                        <p><a href="#" data-toggle="modal" data-target="#myModal3" > Don't have an account?</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- //Modal1 -->
    <!-- Modal2 -->
    <div class="modal fade" id="myModal3" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="signin-form profile">
                        <h3 class="agileinfo_sign">Sign Up</h3>
                        <div class="login-form">
                            <form action="#" method="post">
                                <input type="text" name="name" placeholder="Username" required="">
                                <input type="email" name="email" placeholder="Email" required="">
                                <input type="password" name="password" placeholder="Password" required="">
                                <input type="password" name="password" placeholder="Confirm Password" required="">
                                <input type="submit" value="Sign Up">
                            </form>
                        </div>
                        <p><a href="#"> By clicking Sign Up, I agree to your terms</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"> </div>
    <!-- //Modal2 -->
    <!-- about -->
    <div class="about-top" id="about">
        <div class="container">
            <h3 class="w3l-title">About Us</h3>
            <div class="w3layouts_header">
                <p><i class="fa fa-graduation-cap" aria-hidden="true"></i></p>
            </div>
            <div class="col-md-7 wthree-services-bottom-grids">
                <div class="wthree-services-left">
                    <img width="" src="images/ab1.jpg" alt="">
                </div>
                <div class="wthree-services-right">
                    <img width="500" src="images/ab2.jpg" alt="">
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="col-md-5 wthree-about-grids">
                <h4>Snitch to save a child</h4>
                <a href="#" class="trend-w3l" data-toggle="modal" data-target="#myModal"><span>Read More</span></a>
                <a href="#mail" class="trend-w3l scroll"><span>Get In Touch</span></a>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
    <!-- modal -->
    <div class="modal about-modal w3-agileits fade" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <img src="images/g10.jpg" alt="">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras rutrum iaculis enim, non convallis felis mattis at. Donec fringilla lacus eu pretium rutrum. Cras aliquet congue ullamcorper. Etiam mattis eros eu ullamcorper volutpat. Proin ut dui a urna efficitur varius. uisque molestie cursus mi et congue consectetur adipiscing elit cras rutrum iaculis enim, Lorem ipsum dolor sit amet, non convallis felis mattis at. Maecenas sodales tortor ac ligula ultrices dictum et quis urna. Etiam pulvinar metus neque, eget porttitor massa vulputate. </p>
                </div>
            </div>
        </div>
    </div>
    <!-- //modal -->
    <!-- //about -->
    <!--stats-->
    <div class="stats" id="stats">
        <div class="container">
            <div class="stats-info">
                <div class="col-md-4 col-md-offset-2 stats-grid slideanim">
                    <i class="fa fa-users" aria-hidden="true"></i>
                    <div class='numscroller numscroller-big-bottom' data-slno='1' data-min='0' data-max='{{count($snitches)}}' data-delay='.5' data-increment="1">{{count($snitches)}}</div>

                    <h4 class="stats-info">SNITCHES</h4>
                </div>
                <div class="col-md-4  stats-grid slideanim">
                    <i class="fa fa-book" aria-hidden="true"></i>
                    <div id="reportCount" class='numscroller numscroller-big-bottom' data-slno='1' data-min='0' data-max='{{count($reports)}}' data-delay='.5' data-increment="1">{{count($reports)}}</div>

                    <h4 class="stats-info">REPORTS</h4>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!--//stats-->
    <!-- services -->
    <div class="services" id="getinfo" >
        <div class="container">
            <h3 class="w3l-title">Get Information</h3>
            <div class="w3layouts_header">
                <p><i class="fa fa-graduation-cap" aria-hidden="true"></i></p>
            </div>
            <div class="services-w3ls-row">
                <a href="{{url('law-search')}}">
                <div class="col-xs-3 services-grid agileits-w3layouts">
                        <span class="fa fa-legal" aria-hidden="true"></span>
                        <h6>01</h6>
                        <h5>LAW SEARCH</h5>
                        <p>
                            We have the most recent version of laws that pertain to child labour. Look at it, and get familiar
                            so you know what you can do legally. Let's use the laws we already have.
                        </p>

                    <br><a href="{{url('cra.pdf')}}" class="btn btn-primary">View</a>
                </div>
                </a>

                <a href="{{url('resources')}}">
                <div class="col-xs-3 services-grid agileits-w3layouts">
                    <span class="fa fa-book" aria-hidden="true"></span>
                    <h6>02</h6>
                    <h5>RESOURCES</h5>
                    <p>We have a lot of resources and links to amazing people doing amazing things to try to curb child labour.
                       Know what is available, how you can help them, or have your effort/project listed today.
                    </p>
                    <br><a href="{{url('resources')}}" class="btn btn-primary">View</a>
                </div>
                </a>

                <a href="{{url('cases')}}">
                <div class="col-xs-3 services-grid agileits-w3layouts">
                    <span class="fa fa-book" aria-hidden="true"></span>
                    <h6>03</h6>
                    <h5>CASES</h5>
                    <p>Based on reports that come to us, we make cases citing those reports. Tell you their story and get
                        people to sign petitions that we can use to pass laws to help prevent those sad situations.</p>
                    <br><a href="{{url('cases')}}" class="btn btn-primary">View</a>
                </div>
                </a>

                <a href="{{url('snitchdb')}}">
                <div class="col-xs-3 services-grid agileits-w3layouts">
                    <span class="fa fa-book" aria-hidden="true"></span>
                    <h6>04</h6>
                    <h5>SNITCH DB</h5>
                    <p>Snitch db is our attempt to log all good deeds for kids. The idea is to generate data we can learn from
                    and collate for individuals to use to do innovative things.</p>
                    <br><a href="{{url('snitchdb')}}" class="btn btn-primary">View</a>
                </div>
                </a>


                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
    <!-- //services -->
    <!-- Gallery -->
    <section class="portfolio-w3ls" id="gallery">
        <h3 class="w3l-title">Our Gallery</h3>
        <div class="w3layouts_header">
            <p><i class="fa fa-graduation-cap" aria-hidden="true"></i></p>
        </div>
        <div class="col-md-3 col-xs-3 gallery-grid gallery1">
            <a href="images/g1.jpg" class="swipebox"><img src="images/g1.jpg" class="img-responsive" alt="/">
                <div class="textbox">
                    <h4>scholarly</h4>
                    <p><i class="fa fa-picture-o" aria-hidden="true"></i></p>
                </div>
            </a>
        </div>
        <div class="col-md-3 col-xs-3 gallery-grid gallery1">
            <a href="images/g2.jpg" class="swipebox"><img src="images/g2.jpg" class="img-responsive" alt="/">
                <div class="textbox">
                    <h4>scholarly</h4>
                    <p><i class="fa fa-picture-o" aria-hidden="true"></i></p>
                </div>
            </a>
        </div>
        <div class="col-md-3 col-xs-3 gallery-grid gallery1">
            <a href="images/g3.jpg" class="swipebox"><img src="images/g3.jpg" class="img-responsive" alt="/">
                <div class="textbox">
                    <h4>scholarly</h4>
                    <p><i class="fa fa-picture-o" aria-hidden="true"></i></p>
                </div>
            </a>
        </div>
        <div class="col-md-3 col-xs-3 gallery-grid gallery1">
            <a href="images/g7.jpg" class="swipebox"><img src="images/g7.jpg" class="img-responsive" alt="/">
                <div class="textbox">
                    <h4>scholarly</h4>
                    <p><i class="fa fa-picture-o" aria-hidden="true"></i></p>
                </div>
            </a>
        </div>
        <div class="col-md-3 col-xs-3 gallery-grid gallery1">
            <a href="images/g5.jpg" class="swipebox"><img src="images/g5.jpg" class="img-responsive" alt="/">
                <div class="textbox">
                    <h4>scholarly</h4>
                    <p><i class="fa fa-picture-o" aria-hidden="true"></i></p>
                </div>
            </a>
        </div>
        <div class="col-md-3 col-xs-3 gallery-grid gallery1">
            <a href="images/g6.jpg" class="swipebox"><img src="images/g6.jpg" class="img-responsive" alt="/">
                <div class="textbox">
                    <h4>scholarly</h4>
                    <p><i class="fa fa-picture-o" aria-hidden="true"></i></p>
                </div>
            </a>
        </div>
        <div class="col-md-3 col-xs-3 gallery-grid gallery1">
            <a href="images/g11.jpg" class="swipebox"><img src="images/g11.jpg" class="img-responsive" alt="/">
                <div class="textbox">
                    <h4>scholarly</h4>
                    <p><i class="fa fa-picture-o" aria-hidden="true"></i></p>
                </div>
            </a>
        </div>
        <div class="col-md-3 col-xs-3 gallery-grid gallery1">
            <a href="images/g8.jpg" class="swipebox"><img src="images/g8.jpg" class="img-responsive" alt="/">
                <div class="textbox">
                    <h4>scholarly</h4>
                    <p><i class="fa fa-picture-o" aria-hidden="true"></i></p>
                </div>
            </a>
        </div>
        <div class="col-md-3 col-xs-3 gallery-grid gallery1">
            <a href="images/g9.jpg" class="swipebox"><img src="images/g9.jpg" class="img-responsive" alt="/">
                <div class="textbox">
                    <h4>scholarly</h4>
                    <p><i class="fa fa-picture-o" aria-hidden="true"></i></p>
                </div>
            </a>
        </div>
        <div class="col-md-3 col-xs-3 gallery-grid gallery1">
            <a href="images/g10.jpg" class="swipebox"><img src="images/g10.jpg" class="img-responsive" alt="/">
                <div class="textbox">
                    <h4>scholarly</h4>
                    <p><i class="fa fa-picture-o" aria-hidden="true"></i></p>
                </div>
            </a>
        </div>
        <div class="col-md-3 col-xs-3 gallery-grid gallery1">
            <a href="images/g4.jpg" class="swipebox"><img src="images/g4.jpg" class="img-responsive" alt="/">
                <div class="textbox">
                    <h4>scholarly</h4>
                    <p><i class="fa fa-picture-o" aria-hidden="true"></i></p>
                </div>
            </a>
        </div>
        <div class="col-md-3 col-xs-3 gallery-grid gallery1">
            <a href="images/g12.jpg" class="swipebox"><img src="images/g12.jpg" class="img-responsive" alt="/">
                <div class="textbox">
                    <h4>scholarly</h4>
                    <p><i class="fa fa-picture-o" aria-hidden="true"></i></p>
                </div>
            </a>
        </div>
        <div class="clearfix"> </div>
    </section>
    <!-- //gallery -->
    <!-- team -->
    <div class="team-w3l" id="offerinfo">
        <div class="container">
            <h3 class="w3l-title">Offer Information</h3>
            <div class="w3layouts_header">
                <p><i class="fa fa-graduation-cap" aria-hidden="true"></i></p>
            </div>
            <div class="team-w3l-grid">
                <div class="col-md-6 col-xs-12 about-poleft t1">
                    <div class="about_img"><img src="images/t1.jpg" alt="">
                        <h5>LEARN TO SNITCH</h5>
                        <div class="about_opa">
                            <p>To snitch, fill out the snitch form on our website. More info on our social media
                            pages below</p>
                            <ul class="fb_icons2 text-center">
                                <li><a class="fa fa-facebook" href="#"></a></li>
                                <li><a class="fa fa-twitter" href="#"></a></li>
                                <li><a class="fa fa-google" href="#"></a></li>
                                <li><a class="fa fa-linkedin" href="#"></a></li>
                                <li><a class="fa fa-pinterest-p" href="#"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xs-12 about-poleft t3">
                    <div class="about_img"><img src="images/t3.jpg" alt="">
                        <h5>LEARN TO REPORT</h5>
                        <div class="about_opa">
                            <p>To report follow the detailed guide on our social media</p>
                            <ul class="fb_icons2 text-center">
                                <li><a class="fa fa-facebook" href="#"></a></li>
                                <li><a class="fa fa-twitter" href="#"></a></li>
                                <li><a class="fa fa-google" href="#"></a></li>
                                <li><a class="fa fa-linkedin" href="#"></a></li>
                                <li><a class="fa fa-pinterest-p" href="#"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- //team -->

    <script>


        function updateReport(){
            $.ajax({
                url: "{{url('api/get-reports')}}",
                method: "get",
                success: function (response) {
                    $('#reportCount').text(response);
                },
                error: function (error) {
                    console.log(error);
                }
            });

        }


        $(document).ready(function () {

            setInterval(updateReport,5000);

        });

    </script>

@endsection