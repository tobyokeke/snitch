<?php use Illuminate\Support\Facades\Session; ?>


<div class="col m8 offset-m2">

@if(Session::has('success'))
    <div class="col m10 success card-panel white-text" style="background-color: darkgreen;" align="center">{{Session::get('success')}}</div>
@endif

@if(Session::has('error'))
    <div class="col m10 error card-panel red white-text" align="center">{{Session::get('error')}}</div>
@endif
</div>