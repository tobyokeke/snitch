<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class IntialDb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	
    	Schema::create('locations', function(Blueprint $table){
    	    $table->increments('lid');
    	    $table->string('name');
    	    $table->string('state');
    	    $table->timestamps();
    	});
    	
    	Schema::create('agencies', function(Blueprint $table){
    	    $table->increments('aid');
    	    $table->string('name');
    	    $table->string('description',2000);
    	    $table->string('email');
    	    $table->string('phone');
    	    $table->string('address');
    	    $table->timestamps();
    	});
    	
    	Schema::create('keywords', function(Blueprint $table){
    	    $table->increments('kid');
    	    $table->integer('aid');
    	    $table->string('word');
    	    $table->timestamps();
    	});

    	Schema::create('reports', function(Blueprint $table){
    	    $table->increments('rid');
    	    $table->string('phone');
    	    $table->string('details');
    	    $table->timestamps();
    	});

    	Schema::create('snitches', function(Blueprint $table){
    	    $table->increments('sid');
    	    $table->integer('lid');
    	    $table->string('name');
    	    $table->string('image',1000);
    	    $table->string('file')->nullable();
    	    $table->string('story',5000);
    	    $table->decimal('lat',15,14)->nullable();
    	    $table->decimal('lng',15,14)->nullable();
    	    $table->timestamps();
    	});


    	Schema::create('cases', function(Blueprint $table){
    	    $table->increments('cid');
    	    $table->string('topic');
    	    $table->string('description');
    	});

    	Schema::create('case_snitches', function(Blueprint $table){
    	    $table->increments('csid');
    	    $table->integer('cid');
    	    $table->integer('sid');
    	});

    	Schema::create('case_emails', function(Blueprint $table){
    	    $table->increments('ceid');
    	    $table->integer('cid');
    	    $table->string('name');
    	    $table->string('email');
    	    $table->string('phone');
    	    $table->timestamps();
    	});

	    Schema::create('resources', function(Blueprint $table){
		    $table->increments('resid');
		    $table->string('name');
		    $table->string('story');
		    $table->string('image');
		    $table->string('website');
		    $table->string('phone');
		    $table->string('email');
	    });

	    Schema::create('users', function (Blueprint $table) {
		    $table->increments('uid');
		    $table->string('name');
		    $table->string('email',191)->unique();
		    $table->string('password');
		    $table->rememberToken();
		    $table->timestamps();
	    });

	    Schema::create('password_resets', function (Blueprint $table) {
		    $table->string('email',191)->index();
		    $table->string('token');
		    $table->timestamp('created_at')->nullable();
	    });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('users');
	    Schema::dropIfExists('password_resets');
	    Schema::dropIfExists('locations');
	    Schema::dropIfExists('agencies');
	    Schema::dropIfExists('keywords');
	    Schema::dropIfExists('reports');
	    Schema::dropIfExists('snitches');
	    Schema::dropIfExists('cases');
	    Schema::dropIfExists('case_snitches');
	    Schema::dropIfExists('case_emails');
	    Schema::dropIfExists('resources');
    }
}
